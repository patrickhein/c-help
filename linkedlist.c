#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

/* Functionality of Linked List: 
--> the memory is represented by the struct memory_t, the memory blocks can be either empty or not
--> the list is represented by the nodes, which provide information about the position and place of the empty data blocks */

#define MEMORY_SIZE 100
#define DATA_SIZE 20

int addedBlocks[MEMORY_SIZE]; //pos, size 

struct memory_t {
    bool isEmpty;
} list[100];


struct node {
    int size; //information about the size of the empty memory storage
    int pos; //information about the position of the empty memory storage
    struct node* next; //pointer to the next element in the list
} *head; 
typedef struct node node_t;

void printlist(node_t* node) {
    printf("Currently free blocks:\n");
    while(node != NULL) {
        printf("Position: %d, Size: %d\n", node->pos, node->size);
        node = node->next;
    }
    printf("\n");
}

node_t *create_new_node(int size, int pos) {
    node_t *result = malloc(sizeof(node_t));
    result->size = size;
    result->pos = pos;
    result->next = NULL;
    return result;
}

node_t *insert_at_head(node_t** head_ref, int size, int pos) { 
    node_t *newnode = (struct node*)malloc(sizeof(struct node));
    newnode->size = size;
    newnode->pos = pos;
    newnode->next = (*head_ref);

    (*head_ref) = newnode;

    printf("A new node with the size %d has been inserted at pos %d (head of the list)\n", newnode->size, newnode->pos);
    return newnode;
}

void *insert_after_node(node_t* node_to_insert_after, int size, int pos) {
    node_t *newnode = (struct node*)malloc(sizeof(struct node));

    newnode->pos = pos;
    newnode->size = size;

    newnode->next = node_to_insert_after->next;

    printf("A new node has been inserted at pos %d with the size %d\n", newnode->pos, newnode->size);

    node_to_insert_after->next = newnode;
}

void *delete_right(node_t *node_to_delete_after, int pos) {
    node_t *tmp = node_to_delete_after->next; //tmp = node to delete
    node_to_delete_after->next = node_to_delete_after->next->next; //node next to tmp is now next to list
    free(tmp); //node gets deleted

    printf("The node at position %d has been deleted successfully\n", pos);
    return node_to_delete_after;
}

node_t *find_node_by_size(node_t *head, int size) {
    node_t *tmp = head;
    while(tmp != NULL) {
        if(tmp->size == size) return tmp;
        tmp = tmp->next;
    }
    return NULL;
}

node_t *find_node_by_position(node_t* head, int pos) {
    node_t *tmp = head;
    while(tmp != NULL) {
        if(tmp->pos == pos) return tmp;
        tmp = tmp->next;
    }
    return NULL;
}

node_t *find_prev_node(node_t* head, int deleteDataPos) {
    bool isNeighborFound = false;
    int position = deleteDataPos-1;
    node_t *possibleNeighbor;
    while(false) {
        possibleNeighbor = find_node_by_position(head, position);
        if(possibleNeighbor != NULL) {
            return possibleNeighbor;
        }
        position--;
    }
    return NULL;
}

void insert_data(node_t *head, int dataSize) {
    bool emptyPlaceFound = false;
    node_t *tmp = head;
    node_t *tmp_prev = malloc(sizeof(node_t));
    while(emptyPlaceFound == false) {
        if(tmp->size >= dataSize) {
            emptyPlaceFound = true;
            for(int i=0; i<= dataSize; i++) {
                list[i].isEmpty = false;
            }
            printf("A block with the size of %d has been inserted at pos %d\n", dataSize, tmp->pos);

            addedBlocks[tmp->pos] = dataSize;

            tmp->pos = tmp->pos + dataSize;
            tmp->size = tmp->size - dataSize;

            if(tmp->size == 0) {
                delete_right(tmp_prev, tmp->pos);
            }
            
            printlist(head);

            return;
        }
        tmp_prev = tmp;
        tmp = tmp->next;
    }
    printf("The block with the size of %d couldn't be inserted\n", dataSize);
} 

void delete_data(node_t *head, int deleteDataSize, int deleteDataPos) {
    int tmpPos = deleteDataPos;

    for(int i=0; i<tmpPos; i++) {
        list[tmpPos].isEmpty = true;
        tmpPos++;
    }

    printf("A block with the size of %d has been successfully deleted from position %d\n", deleteDataSize, deleteDataPos);

    if(deleteDataPos < head->pos) {
        insert_at_head(&head, deleteDataSize, deleteDataPos);
    }
    else {
        insert_after_node(find_prev_node(head, deleteDataPos), deleteDataSize, deleteDataPos);
    }

    addedBlocks[deleteDataPos] = 0;

    printlist(head);
}

int main() {
    head = (struct node *)malloc(sizeof(struct node));
    head = create_new_node(MEMORY_SIZE, 0);
    node_t *tmp;

    for(int i=0; i<MEMORY_SIZE; i++) {
        list[i].isEmpty = true;
    }

    int dataSize, deleteDataSize, deleteDataPos;
    srand(time(0)); 

    for(int i=0; i<100; i++) {
        if(i>3 && (i%2 == 0)) {
            bool blockFound = false;
            while(blockFound == false) {
                deleteDataPos = (rand() % (100 - 0 + 1)) + 0;
                if(addedBlocks[deleteDataPos] != 0) {
                    deleteDataSize = addedBlocks[deleteDataPos];
                    delete_data(head, deleteDataSize, deleteDataPos);
                    blockFound = true;
                }
            }
        }
        else {
            dataSize = (int)(1.0 + DATA_SIZE * rand() / RAND_MAX); //generate random number between 1 and 20
            insert_data(head, dataSize);
        }
    }

    return 0;
}
